-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Июн 15 2018 г., 09:11
-- Версия сервера: 10.1.32-MariaDB
-- Версия PHP: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `beltelecom_db`
--

-- --------------------------------------------------------

--
-- Структура таблицы `equip`
--

CREATE TABLE `equip` (
  `id` int(11) NOT NULL COMMENT 'id',
  `date_get` date NOT NULL COMMENT 'Дата получения',
  `types` varchar(255) NOT NULL COMMENT 'Тип устройства',
  `serial_Number` varchar(255) NOT NULL COMMENT 'серийный номер',
  `mac` varchar(255) NOT NULL COMMENT 'MAC',
  `number_Phone` int(11) NOT NULL COMMENT 'Номер телефона',
  `issuing` date NOT NULL COMMENT 'Дата выдачи',
  `FIO` varchar(255) NOT NULL COMMENT 'ФИО',
  `type_result` varchar(255) NOT NULL COMMENT 'Тип возврашаем.',
  `mac_Result` varchar(11) NOT NULL COMMENT 'MAC возврошен',
  `date_return` date NOT NULL COMMENT 'Дата возврата',
  `date_cpu` date NOT NULL COMMENT 'дата передачи цпу',
  `comment` text NOT NULL COMMENT 'Коментарий',
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `equip`
--

INSERT INTO `equip` (`id`, `date_get`, `types`, `serial_Number`, `mac`, `number_Phone`, `issuing`, `FIO`, `type_result`, `mac_Result`, `date_return`, `date_cpu`, `comment`, `status`) VALUES
(1, '2018-06-06', 'M200A', '234356', 'DF12HG78', 0, '0000-00-00', '', '', '', '0000-00-00', '0000-00-00', '', 1),
(2, '2018-06-06', 'M200A', '234356', 'DF12HG78', 0, '0000-00-00', '', '', '', '0000-00-00', '0000-00-00', '', 1),
(3, '2018-06-14', '21321213', '21312323', '2133232112', 0, '0000-00-00', '', '', '', '0000-00-00', '0000-00-00', '', 1),
(4, '2018-06-14', 'HG532e', '3205235', 'v235aeg235', 0, '0000-00-00', '', '', '', '0000-00-00', '0000-00-00', '', 1),
(5, '2018-06-14', 'HG532e', '3205235', 'v235aeg235', 0, '0000-00-00', '', '', '', '0000-00-00', '0000-00-00', '', 1),
(6, '2018-06-14', 'HG532e', '3205235', 'v235aeg235', 0, '0000-00-00', '', '', '', '0000-00-00', '0000-00-00', 'Wifi', 1),
(7, '2018-06-07', 'ÐŸÑƒÐ»ÑŒÑ‚', 'dssdsds', 'sdsdsds', 0, '0000-00-00', '', '', '', '0000-00-00', '0000-00-00', '', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `spec`
--

CREATE TABLE `spec` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sur_name` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `equip`
--
ALTER TABLE `equip`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `spec`
--
ALTER TABLE `spec`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `equip`
--
ALTER TABLE `equip`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id', AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `spec`
--
ALTER TABLE `spec`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
