<?php
include_once "config/DB.php";
include_once "system/classes/DB.php";
include "system/function.php";
$db = new DB(HOST,USER, PASS, DBNAME);

?>


<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/beltelecom/style.css">
    <title>Beltelecom</title>
</head>
<body>
<header>
    <ul class="menu">
        <li><a href="/beltelecom/index.php">Главная</a></li>
        <li><a href="/beltelecom/equip.php">Оборудование</a></li>
        <li><a href="/beltelecom/spec.php">Специалисты</a></li>
        <li><a href="/beltelecom/check.php">Проверка задолжености</a></li>
        <li><a href="/beltelecom/formPrint.php">Печатная форма</a></li>
        <li><a href="/beltelecom/addEquip.php">Добавить оборудование</a></li>
        <li><a href='index.php?f=logout'>Выход</a></li>
    </ul>
</header>
<section>
    <h1>Оборудование</h1>
</section>

<table border="1" width="100%">
    <thead>
    <tr class="loop">
        <th><a class="filter" href="#">№</a></th>
        <th><a class="filter" href="#">Дата<br>получения</a></th>
        <th><a class="filter" href="#">Тип<br>устройство</a></th>
        <th><a class="filter" href="#">Серийный номер</a></th>
        <th><a class="filter" href="#">MAC адрес</a></th>
        <th><a class="filter" href="#">№ Або-<br>нента</a></th>
        <th><a class="filter" href="#">Дата<br>выдачи</a></th>
        <th><a class="filter" href="#">ФИО<br>Специалиста</a></th>
        <th><a class="filter" href="#">Возр.<br>устройство</a></th>
        <th><a class="filter" href="#">МАС возр.<br>устройства</a></th>
        <th><a class="filter" href="#">Дата<br>возврата</a></th>
        <th><a class="filter" href="#">Дата<br>передачи</a></th>
        <th><a class="filter" href="#">Примечание</a></th>
    </tr>
    </thead>
    <tbody>
        <tr>
            <td>0001</td>
            <td>10.06.2016</td>
            <td>
                <select>
                    <option>M200-A</option>
                    <option>M200-B</option>
                    <option>HG532e</option>
                    <option>HG520</option>
                    <option>HG530c</option>
                </select>
            </td>
            <td><?php $query = "SELECT serial_Number FROM `equip` WHERE mac == DF12HG78 "; ?> </td>
            <td>C070091DF2FD</td>
            <td>462985</td>
            <td>10.06.2016</td>
            <td>
                <select>
                    <option>Бартошук В.</option>
                    <option>Корако В.</option>
                    <option>Просенков М.</option>
                    <option>Кузин А.</option>
                    <option>Лацевич Э.</option>
                </select>
            </td>
            <td>
                <select>
                    <option>M200-A</option>
                    <option>M200-B</option>
                    <option>HG532e</option>
                    <option>HG520</option>
                    <option>HG530c</option>
                </select>
            </td>
            <td>C070091DF2FD</td>
            <td>10.06.2016</td>
            <td>10.06.2016</td>
            <td>Слабое покрытие Wifi</td>
        </tr>
    </tbody>

</table>

</body>
</html>