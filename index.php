<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/beltelecom/style.css">
    <title>Beltelecom</title>
</head>
<body>
<header>
    <ul class="menu">
        <li><a href="/beltelecom/index.php">Главная</a></li>
        <li><a href="/beltelecom/equip.php">Оборудование</a></li>
        <li><a href="/beltelecom/spec.php">Специалисты</a></li>
        <li><a href="/beltelecom/check.php">Проверка задолжености</a></li>
        <li><a href="/beltelecom/formPrint.php">Печатная форма</a></li>
        <li><a href="/beltelecom/addEquip.php">Добавить оборудование</a></li>
        <li><a href='index.php?f=logout'>Выход</a></li>
    </ul>
</header>
<section>
    <h1>Главная</h1>
</section>
</body>
</html>