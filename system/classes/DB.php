<?php
/**
 * Created by PhpStorm.
 * User: Bartoshuk Vadim
 * Date: 10.06.2018
 * Time: 19:42
 */

class DB
{
    protected $connection;

    public function __construct($host, $login, $pass, $dbName)
    {
        try{
            $this->connection = mysqli_connect($host, $login, $pass, $dbName);
        }
        catch (Exception $error) {
            echo "Не удалось подключиться DB " . $error;
        }
    }

    public function isConnect()
    {
        return $this->connection ? $this->connection : false;
    }

    public function query($sql)
    {
        $result = $this->connection->query($sql);

        if(!$result){
            echo "Запрос не удался\r\n";
            echo "Номер ошибки: " . $this->connection->errno . "\r\n";
            echo "Ошибка " . $this->connection->error;
            exit;
        }

        if(is_bool($result)){
            return $result;
        }

        $data = array();

        while($row = $result->fetch_assoc()) {
            $data[] = $row;
        }

        return $data;
    }

}