<?php
/**
 * Created by PhpStorm.
 * User: Bartoshuk Vadim
 * Date: 11.06.2018
 * Time: 22:31
 */

function Dump($value) {
    echo "<pre>";
    var_dump($value);
    echo "</pre>";
}

function getTypes() {
    return ['HG8245H', 'H108N', 'Пульт', 'H267N', 'B700 V5', 'F660v5', 'HG552e', 'H208L', 'H208LB', 'H208N', 'HG532', 'HG8245H', 'HG8245A', 'HG552f', 'HG552d', 'PON-AT-3', 'PON-AT-4', 'M200A', 'M200B', 'STB-600', 'Сплиттер', 'БП', 'STB-700'];
}