<?php
include_once "config/DB.php";
include_once "system/classes/DB.php";
include "system/function.php";
$db = new DB(HOST,USER, PASS, DBNAME);
$types = getTypes();

if(isset($_GET['id'])) {
    $equip = $db->query("SELECT * FROM equip WHERE id = " . (int)$_GET['id']);
} else {
    $equips = $db->query("SELECT * FROM equip WHERE 1");
}
?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/beltelecom/style.css">
    <title>Beltelecom</title>
</head>
<body>
<header>
    <header>
        <ul class="menu">
            <li><a href="/beltelecom/index.php">Главная</a></li>
            <li><a href="/beltelecom/equip.php">Оборудование</a></li>
            <li><a href="/beltelecom/spec.php">Специалисты</a></li>
            <li><a href="/beltelecom/check.php">Проверка задолжености</a></li>
            <li><a href="/beltelecom/formPrint.php">Печатная форма</a></li>
            <li><a href="/beltelecom/addEquip.php">Добавить оборудование</a></li>
            <li><a href='index.php?f=logout'>Выход</a></li>
        </ul>
    </header>
    <h1>Оборудование</h1>

    <?php if(isset($_GET['id'])) { ?>
        <form action="/beltelecom/" method="post">
            <div class="form_equip">
                <div class="group-input">
                    <label for="type">Тип оборудования</label>
                    <select name="type" id="type" class="select_control">
                        <?php foreach ($types as $key => $value) { ?>
                            <option <?php echo $value == $equip[0]['types'] ? 'selected="selected"' : ''?> value="<?=$value?>"><?=$value?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="group-input">
                    <label for="serial_Number">Серийный номер</label>
                    <input type="text" name="serial_Number" value="<?=$equip[0]['serial_Number']?>" id="serial_Number">
                </div>
                <div class="group-input">
                    <label for="mac">MAC</label>
                    <input type="text" name="mac" value="<?=$equip[0]['mac']?>" id="mac">
                </div>
                <div class="group-input">
                    <label for="date_get">Дата</label>
                    <input type="date" name="date_get" value="<?=$equip[0]['date_get']?>" id="date_get">
                </div>

                <input type="submit" value="Обновить" class="btn btn-save">
            </div>
        </form>
    <?php } else { ?>
        <table border="1" width="100%">
    <thead>
        <tr class="loop">
            <th><a class="filter" href="#">№</a></th>
            <th><a class="filter" href="#">Дата<br>получения</a></th>
            <th><a class="filter" href="#">Тип<br>устройство</a></th>
            <th><a class="filter" href="#">Серийный номер</a></th>
            <th><a class="filter" href="#">MAC адрес</a></th>
            <th><a class="filter" href="#">№ Або-<br>нента</a></th>
            <th><a class="filter" href="#">Дата<br>выдачи</a></th>
            <th><a class="filter" href="#">ФИО<br>Специалиста</a></th>
            <th><a class="filter" href="#">Возр.<br>устройство</a></th>
            <th><a class="filter" href="#">МАС возр.<br>устройства</a></th>
            <th><a class="filter" href="#">Дата<br>возврата</a></th>
            <th><a class="filter" href="#">Дата<br>передачи</a></th>
            <th><a class="filter" href="#">Примечание</a></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($equips as $item) { ?>
            <tr>
                <td><?=$item['id']?></td>
                <td><?=$item['date_get']?></td>
                <td><?=$item['types']?></td>
                <td><?=$item['serial_Number']?></td>
                <td><?=$item['mac']?></td>
                <td><?=$item['number_Phone']?></td>
                <td><?=$item['issuing']?></td>
                <td><?=$item['FIO']?></td>
                <td><?=$item['type_result']?></td>
                <td><?=$item['mac_Result']?></td>
                <td><?=$item['date_return']?></td>
                <td><?=$item['date_cpu']?></td>
                <td><?=$item['comment']?></td>
                <td><a class="a_action" href="?id=<?=$item['id']?>">-></a></td>
            </tr>
        <?php } ?>
    </tbody>

</table>
    <?php } ?>
</body>
</html>