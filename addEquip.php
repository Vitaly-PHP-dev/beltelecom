<?php
include_once "config/DB.php";
include_once "system/classes/DB.php";
include "system/function.php";
$db = new DB(HOST,USER, PASS, DBNAME);
$types = getTypes();
?>

<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/beltelecom/style.css">
    <title>Beltelecom</title>
</head>
<body>
<header>
    <ul class="menu">
        <li><a href="/beltelecom/index.php">Главная</a></li>
        <li><a href="/beltelecom/equip.php">Оборудование</a></li>
        <li><a href="/beltelecom/spec.php">Специалисты</a></li>
        <li><a href="/beltelecom/check.php">Проверка задолжености</a></li>
        <li><a href="/beltelecom/formPrint.php">Печатная форма</a></li>
        <li><a href="/beltelecom/addEquip.php">Добавить оборудование</a></li>
        <li><a href='index.php?f=logout'>Выход</a></li>
    </ul>
</header>
<section>
    <h1>Добавить оборудование в базу</h1>
    <a href="/beltelecom/addEquip.php"></a>

    <form action="/beltelecom/addEquipPOST.php" method="post">
        <div class="form_equip">
            <div class="group-input">
                <label for="type">Тип оборудования</label>
                <select name="type" id="type" class="select_control">
                    <?php foreach ($types as $key => $value) { ?>
                        <option value="<?=$value?>"><?=$value?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="group-input">
                <label for="serial_Number">Серийный номер</label>
                <input type="text" name="serial_Number" value="" id="serial_Number">
            </div>
            <div class="group-input">
                <label for="mac">MAC</label>
                <input type="text" name="mac" value="" id="mac">
            </div>
            <div class="group-input">
                <label for="date_get">Дата</label>
                <input type="date" name="date_get" value="" id="date_get">
            </div>

            <input type="submit" value="Добавить" class="btn btn-save">
        </div>
    </form>
</section>

</body>
</html>